--- camchenry - MIT License
-- https://github.com/camchenry/sock.lua

local sock = {}
local enet = require("enet")
local bitser = require("libraries/bitser")
local Logger = {}
local Logger_mt = {__index = Logger}
local Listener = {}
local Listener_mt = {__index = Listener}
local Server = {}
local Server_mt = {__index = Server}
local Client = {}
local Client_mt = {__index = Client}

local function zipTable(items, keys, event) local data = {} for i, value in ipairs(items) do local key = keys[i] if not key then error("Event '"..event.."' missing data key. Is the schema different between server and client?") end data[key] = value end return data end
local function isValidSendMode(mode) for _, validMode in ipairs(sock.SEND_MODES) do if mode == validMode then return true end end return false end
local function newLogger(source) local logger = setmetatable({ source = source, messages = {}, shortenLines = true, printEventData = false, printErrors = true, printWarnings = true, }, Logger_mt) return logger end
local function newListener() local listener = setmetatable({ triggers = {}, schemas= {}, }, Listener_mt) return listener end 

sock.newServer = function(address, port, maxPeers, maxChannels, inBandwidth, outBandwidth) address = address or "localhost" port = port or 22122 maxPeers = maxPeers or 64 maxChannels= maxChannels or 1 inBandwidth= inBandwidth or 0 outBandwidth = outBandwidth or 0 local server = setmetatable({ address = address, port = port, host = nil, messageTimeout = 0, maxChannels= maxChannels, maxPeers = maxPeers, sendMode = "reliable", defaultSendMode = "reliable", sendChannel= 0, defaultSendChannel = 0, peers = {}, clients = {}, listener = newListener(), logger= newLogger("SERVER"), serialize = nil, deserialize= nil, packetsSent= 0, packetsReceived = 0, }, Server_mt) server.host = enet.host_create(server.address .. ":" .. server.port, server.maxPeers, server.maxChannels) if not server.host then error("Failed to create the host. Is there another server running on :"..server.port.."?") end server:setBandwidthLimit(inBandwidth, outBandwidth) server:setSerialization(bitser.dumps, bitser.loads) return server end
sock.newClient = function(serverOrAddress, port, maxChannels) serverOrAddress = serverOrAddress or "localhost" port = port or 22122 maxChannels= maxChannels or 1 local client = setmetatable({ address = nil, port = nil, host = nil, connection = nil, connectId = nil, messageTimeout = 0, maxChannels= maxChannels, sendMode = "reliable", defaultSendMode = "reliable", sendChannel= 0, defaultSendChannel = 0, listener = newListener(), logger= newLogger("CLIENT"), serialize = nil, deserialize= nil, packetsReceived = 0, packetsSent= 0, }, Client_mt) if port ~= nil and type(port) == "number" and serverOrAddress ~= nil and type(serverOrAddress) == "string" then client.address = serverOrAddress client.port = port client.host = enet.host_create() elseif type(serverOrAddress) == "userdata" then client.connection = serverOrAddress client.connectId = client.connection:connect_id() end client:setSerialization(bitser.dumps, bitser.loads) return client end

sock.CONNECTION_STATES = {
    "disconnected", -- Disconnected from the server.
    "connecting",-- In the process of connecting to the server.
    "acknowledging_connect", --
    "connection_pending", --
    "connection_succeeded",--
    "connected", -- Successfully connected to the server.
    "disconnect_later", -- Disconnecting, but only after sending all queued packets.
    "disconnecting", -- In the process of disconnecting from the server.
    "acknowledging_disconnect", --
    "zombie", --
    "unknown", --
}

--- States that represent the client connecting to a server.
sock.CONNECTING_STATES = {
    "connecting",-- In the process of connecting to the server.
    "acknowledging_connect", --
    "connection_pending", --
    "connection_succeeded",--
}

--- States that represent the client disconnecting from a server.
sock.DISCONNECTING_STATES = {
    "disconnect_later", -- Disconnecting, but only after sending all queued packets.
    "disconnecting", -- In the process of disconnecting from the server.
    "acknowledging_disconnect", --
}

--- Valid modes for sending messages.
sock.SEND_MODES = {
    "reliable",-- Message is guaranteed to arrive, and arrive in the order in which it is sent.
    "unsequenced", -- Message has no guarantee on the order that it arrives.
    "unreliable", -- Message is not guaranteed to arrive.
}

function Logger:log(event, data) local time = os.date("%X") local shortLine = ("[%s] %s"):format(event, data) local fullLine = ("[%s][%s][%s] %s"):format(self.source, time, event, data) local line = fullLine if self.shortenLines then line = shortLine end if self.printEventData then print(line) elseif self.printErrors and event == "error" then print(line) elseif self.printWarnings and event == "warning" then print(line) end table.insert(self.messages, fullLine) end
---
function Listener:addCallback(event, callback) if not self.triggers[event] then self.triggers[event] = {} end table.insert(self.triggers[event], callback) return callback end
function Listener:removeCallback(callback) for _, triggers in pairs(self.triggers) do for i, trigger in pairs(triggers) do if trigger == callback then table.remove(triggers, i) return true end end end return false end
function Listener:setSchema(event, schema) self.schemas[event] = schema end
function Listener:trigger(event, data, client) if self.triggers[event] then for _, trigger in pairs(self.triggers[event]) do if self.schemas[event] then data = zipTable(data, self.schemas[event], event) end trigger(data, client) end return true else return false end end
---
function Server:update() local event = self.host:service(self.messageTimeout) while event do if event.type == "connect" then local eventClient = sock.newClient(event.peer) eventClient:setSerialization(self.serialize, self.deserialize) table.insert(self.peers, event.peer) table.insert(self.clients, eventClient) self:_activateTriggers("connect", event.data, eventClient) self:log(event.type, tostring(event.peer) .. " connected") elseif event.type == "receive" then local eventName, data = self:__unpack(event.data) local eventClient = self:getClient(event.peer) self:_activateTriggers(eventName, data, eventClient) self:log(eventName, data) elseif event.type == "disconnect" then for i, peer in pairs(self.peers) do if peer == event.peer then table.remove(self.peers, i) end end local eventClient = self:getClient(event.peer) for i, client in pairs(self.clients) do if client == eventClient then table.remove(self.clients, i) end end self:_activateTriggers("disconnect", event.data, eventClient) self:log(event.type, tostring(event.peer) .. " disconnected") end event = self.host:service(self.messageTimeout) end end
function Server:__unpack(data) if not self.deserialize then self:log("error", "Can't deserialize message: deserialize was not set") error("Can't deserialize message: deserialize was not set") end local message = self.deserialize(data) local eventName, data = message[1], message[2] return eventName, data end
function Server:__pack(event, data) local message = {event, data} local serializedMessage if not self.serialize then self:log("error", "Can't serialize message: serialize was not set") error("Can't serialize message: serialize was not set") end if type(data) == "userdata" and data.type and data:typeOf("Data") then message[2] = data:getString() serializedMessage = self.serialize(message) else serializedMessage = self.serialize(message) end return serializedMessage end
function Server:sendToAllBut(client, event, data) local serializedMessage = self:__pack(event, data) for _, p in pairs(self.peers) do if p ~= client.connection then self.packetsSent = self.packetsSent + 1 p:send(serializedMessage, self.sendChannel, self.sendMode) end end self:resetSendSettings() end
function Server:sendToAll(event, data) local serializedMessage = self:__pack(event, data) self.packetsSent = self.packetsSent + #self.peers self.host:broadcast(serializedMessage, self.sendChannel, self.sendMode) self:resetSendSettings() end
function Server:sendToPeer(peer, event, data) local serializedMessage = self:__pack(event, data) self.packetsSent = self.packetsSent + 1 peer:send(serializedMessage, self.sendChannel, self.sendMode) self:resetSendSettings() end
function Server:on(event, callback) return self.listener:addCallback(event, callback) end
function Server:_activateTriggers(event, data, client) local result = self.listener:trigger(event, data, client) self.packetsReceived = self.packetsReceived + 1 if not result then self:log("warning", "Tried to activate trigger: '" .. tostring(event) .. "' but it does not exist.") end end
function Server:removeCallback(callback) return self.listener:removeCallback(callback) end
function Server:log(event, data) return self.logger:log(event, data) end
function Server:resetSendSettings() self.sendMode = self.defaultSendMode self.sendChannel = self.defaultSendChannel end
function Server:enableCompression() return self.host:compress_with_range_coder() end
function Server:destroy() self.host:destroy() end
function Server:setSendMode(mode) if not isValidSendMode(mode) then self:log("warning", "Tried to use invalid send mode: '" .. mode .. "'. Defaulting to reliable.") mode = "reliable" end self.sendMode = mode end
function Server:setDefaultSendMode(mode) if not isValidSendMode(mode) then self:log("error", "Tried to set default send mode to invalid mode: '" .. mode .. "'") error("Tried to set default send mode to invalid mode: '" .. mode .. "'") end self.defaultSendMode = mode end
function Server:setSendChannel(channel) if channel > (self.maxChannels - 1) then self:log("warning", "Tried to use invalid channel: " .. channel .. " (max is " .. self.maxChannels - 1 .. "). Defaulting to 0.") channel = 0 end self.sendChannel = channel end
function Server:setDefaultSendChannel(channel) self.defaultSendChannel = channel end
function Server:setSchema(event, schema) return self.listener:setSchema(event, schema) end
function Server:setBandwidthLimit(incoming, outgoing) return self.host:bandwidth_limit(incoming, outgoing) end
function Server:setMaxChannels(limit) self.host:channel_limit(limit) end
function Server:setMessageTimeout(timeout) self.messageTimeout = timeout end
function Server:setSerialization(serialize, deserialize) assert(type(serialize) == "function", "Serialize must be a function, got: '"..type(serialize).."'") assert(type(deserialize) == "function", "Deserialize must be a function, got: '"..type(deserialize).."'") self.serialize = serialize self.deserialize = deserialize end
function Server:getClient(peer) for _, client in pairs(self.clients) do if peer == client.connection then return client end end end
function Server:getClientByConnectId(connectId) for _, client in pairs(self.clients) do if connectId == client.connectId then return client end end end
function Server:getClientByIndex(index) for _, client in pairs(self.clients) do if index == client:getIndex() then return client end end end
function Server:getPeerByIndex(index) return self.host:get_peer(index) end 
function Server:getTotalSentData() return self.host:total_sent_data() end 
function Server:getTotalReceivedData() return self.host:total_received_data() end
function Server:getTotalSentPackets() return self.packetsSent end 
function Server:getTotalReceivedPackets() return self.packetsReceived end 
function Server:getLastServiceTime() return self.host:service_time() end 
function Server:getMaxPeers() return self.maxPeers end 
function Server:getMaxChannels() return self.maxChannels end 
function Server:getMessageTimeout() return self.messageTimeout end 
function Server:getSocketAddress() return self.host:get_socket_address() end 
function Server:getSendMode() return self.sendMode end
function Server:getDefaultSendMode() return self.defaultSendMode end 
function Server:getAddress() return self.address end 
function Server:getPort() return self.port end 
function Server:getClients() return self.clients end 
function Server:getClientCount() return #self.clients end
---
function Client:update() local event = self.host:service(self.messageTimeout) while event do if event.type == "connect" then self:_activateTriggers("connect", event.data) self:log(event.type, "Connected to " .. tostring(self.connection)) elseif event.type == "receive" then local eventName, data = self:__unpack(event.data) self:_activateTriggers(eventName, data) self:log(eventName, data) elseif event.type == "disconnect" then self:_activateTriggers("disconnect", event.data) self:log(event.type, "Disconnected from " .. tostring(self.connection)) end event = self.host:service(self.messageTimeout) end end
function Client:connect(code) self.connection = self.host:connect(self.address .. ":" .. self.port, self.maxChannels, code) self.connectId = self.connection:connect_id() end
function Client:disconnect(code) code = code or 0 self.connection:disconnect(code) end
function Client:disconnectLater(code) code = code or 0 self.connection:disconnect_later(code) end
function Client:disconnectNow(code) code = code or 0 self.connection:disconnect_now(code) end
function Client:reset() if self.connection then self.connection:reset() end end
function Client:__unpack(data) if not self.deserialize then self:log("error", "Can't deserialize message: deserialize was not set") error("Can't deserialize message: deserialize was not set") end local message = self.deserialize(data) local eventName, data = message[1], message[2] return eventName, data end
function Client:__pack(event, data) local message = {event, data} local serializedMessage if not self.serialize then self:log("error", "Can't serialize message: serialize was not set") error("Can't serialize message: serialize was not set") end if type(data) == "userdata" and data.type and data:typeOf("Data") then message[2] = data:getString() serializedMessage = self.serialize(message) else serializedMessage = self.serialize(message) end return serializedMessage end
function Client:send(event, data) local serializedMessage = self:__pack(event, data) self.connection:send(serializedMessage, self.sendChannel, self.sendMode) self.packetsSent = self.packetsSent + 1 self:resetSendSettings() end
function Client:on(event, callback) return self.listener:addCallback(event, callback) end
function Client:_activateTriggers(event, data) local result = self.listener:trigger(event, data) self.packetsReceived = self.packetsReceived + 1 if not result then self:log("warning", "Tried to activate trigger: '" .. tostring(event) .. "' but it does not exist.") end end
function Client:removeCallback(callback) return self.listener:removeCallback(callback) end
function Client:log(event, data) return self.logger:log(event, data) end
function Client:resetSendSettings() self.sendMode = self.defaultSendMode self.sendChannel = self.defaultSendChannel end
function Client:enableCompression() return self.host:compress_with_range_coder() end
function Client:setSendMode(mode) if not isValidSendMode(mode) then self:log("warning", "Tried to use invalid send mode: '" .. mode .. "'. Defaulting to reliable.") mode = "reliable" end self.sendMode = mode end
function Client:setDefaultSendMode(mode) if not isValidSendMode(mode) then self:log("error", "Tried to set default send mode to invalid mode: '" .. mode .. "'") error("Tried to set default send mode to invalid mode: '" .. mode .. "'") end self.defaultSendMode = mode end
function Client:setSendChannel(channel) if channel > (self.maxChannels - 1) then self:log("warning", "Tried to use invalid channel: " .. channel .. " (max is " .. self.maxChannels - 1 .. "). Defaulting to 0.") channel = 0 end self.sendChannel = channel end
function Client:setDefaultSendChannel(channel) self.defaultSendChannel = channel end
function Client:setSchema(event, schema) return self.listener:setSchema(event, schema) end
function Client:setMaxChannels(limit) self.host:channel_limit(limit) end
function Client:setMessageTimeout(timeout) self.messageTimeout = timeout end
function Client:setBandwidthLimit(incoming, outgoing) return self.host:bandwidth_limit(incoming, outgoing) end
function Client:setPingInterval(interval) if self.connection then self.connection:ping_interval(interval) end end
function Client:setThrottle(interval, acceleration, deceleration) interval = interval or 5000 acceleration = acceleration or 2 deceleration = deceleration or 2 if self.connection then self.connection:throttle_configure(interval, acceleration, deceleration) end end
function Client:setTimeout(limit, minimum, maximum) limit = limit or 32 minimum = minimum or 5000 maximum = maximum or 30000 if self.connection then self.connection:timeout(limit, minimum, maximum) end end
function Client:setSerialization(serialize, deserialize) self.serialize = serialize; self.deserialize = deserialize end
function Client:isConnecting() local inConnectingState = false for _, state in ipairs(sock.CONNECTING_STATES) do if state == self:getState() then inConnectingState = true break end end return self.connection ~= nil and inConnectingState end
function Client:isDisconnecting() local inDisconnectingState = false for _, state in ipairs(sock.DISCONNECTING_STATES) do if state == self:getState() then inDisconnectingState = true break end end return self.connection ~= nil and inDisconnectingState end
function Client:isConnected() return self.connection ~= nil and self:getState() == "connected" end
function Client:isDisconnected() return self.connection ~= nil and self:getState() == "disconnected" end
function Client:getTotalSentData() return self.host:total_sent_data() end 
function Client:getTotalReceivedData() return self.host:total_received_data() end
function Client:getTotalSentPackets() return self.packetsSent end
function Client:getTotalReceivedPackets() return self.packetsReceived end 
function Client:getLastServiceTime() return self.host:service_time() end 
function Client:getMaxChannels() return self.maxChannels end 
function Client:getMessageTimeout() return self.messageTimeout end 
function Client:getRoundTripTime() if self.connection then return self.connection:round_trip_time() end end 
function Client:getConnectId() if self.connection then return self.connection:connect_id() end end 
function Client:getState() if self.connection then return self.connection:state() end end 
function Client:getIndex() if self.connection then return self.connection:index() end end
function Client:getSocketAddress() return self.host:get_socket_address() end
function Client:getPeerByIndex(index) return self.host:get_peer(index) end
function Client:getSendMode() return self.sendMode end
function Client:getDefaultSendMode() return self.defaultSendMode end
function Client:getAddress() return self.address end
function Client:getPort() return self.port end

return sock