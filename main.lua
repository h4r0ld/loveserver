local sock = require("libraries/sock")
local server = sock.newServer("*", 1337)

function love.load()
    server:on("connect", function(data, client)
        print(client.connectId .. " connected")
        client:send("playerNum", client:getIndex())
    end)
    
    server:on("disconnect", function(data, client)
        print(client.connectId .. " disconnected")
    end)

    server:on("up", function(data, client)
        print(client.connectId .. " up")
    end)
    server:on("down", function(data, client)
        print(client.connectId .. " down")
    end)
    server:on("left", function(data, client)
        print(client.connectId .. " left")
    end)
    server:on("right", function(data, client)
        print(client.connectId .. " right")
    end)

    tickRate = 1/60
    tick = 0
end

function love.update(dt)
    server:update()
end
