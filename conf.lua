function love.conf(t)
    t.console = true
    t.window = nil
  
    t.modules.joystick = false
    t.modules.keyboard = false
    t.modules.mouse = false
    t.modules.touch = false
    t.modules.physics = false
    t.modules.graphics = false
    t.modules.audio = false
    t.modules.sound = false
    t.modules.font = false
    t.modules.video = false
    t.modules.window = false
end